==========
Quickstart
==========

A friendly message about how easy it is to use pop-create-idem

Getting Started
===============

First, install `pop-create-idem` this is an extension of `pop-create` that
is designed to easily create boilerplate code for idem-cloud projects.

.. code-block:: bash

    pip3 install pop-create-idem

You now have access to the `pop-create` command with the `idem-cloud` subcommand.

To create a new idem-cloud project, run:

.. code-block:: bash

    pop-create idem-cloud --directory /path/to/new/project --project-name=idem-{my_cloud} --simple-cloud-name={my_cloud}

Automatically generating exec modules
=====================================

TODO

Authenticating with acct
========================

TODO

Writing state modules
======================

TODO

Using the idem CLI
==================

TODO

Enjoy!

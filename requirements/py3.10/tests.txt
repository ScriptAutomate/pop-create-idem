#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.10/tests.txt requirements/tests.in
#
acct==6
    # via idem
aiofiles==0.4.0
    # via
    #   acct
    #   aiologger
aiologger[aiofiles]==0.6.0
    # via pop-config
arrow==1.0.3
    # via jinja2-time
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==20.3.0
    # via pytest
binaryornot==0.4.4
    # via cookiecutter
certifi==2020.12.5
    # via requests
cffi==1.14.5
    # via cryptography
chardet==4.0.0
    # via
    #   binaryornot
    #   requests
click==7.1.2
    # via cookiecutter
colored==1.4.2
    # via rend
cookiecutter==1.7.2
    # via pop-create
cryptography==3.4.7
    # via acct
dict-toolbox==2
    # via
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
idem==11
    # via -r requirements/base.txt
idna==2.10
    # via requests
iniconfig==1.1.1
    # via pytest
jinja2-time==0.2.0
    # via cookiecutter
jinja2==2.11.3
    # via
    #   cookiecutter
    #   idem
    #   jinja2-time
    #   rend
jsonnet==0.17.0
    # via -r requirements/base.txt
markupsafe==1.1.1
    # via
    #   cookiecutter
    #   jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.2
    # via
    #   acct
    #   dict-toolbox
    #   pop
    #   pytest-salt-factories
openapi3==1.3.0
    # via -r requirements/base.txt
packaging==20.9
    # via pytest
pluggy==0.13.1
    # via pytest
pop-config==6.11
    # via
    #   pop
    #   pytest-pop
pop-create==7
    # via -r requirements/base.txt
pop==17.1
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-create
    #   pytest-pop
    #   rend
poyo==0.5.0
    # via cookiecutter
psutil==5.8.0
    # via pytest-salt-factories
py==1.10.0
    # via pytest
pycparser==2.20
    # via cffi
pyparsing==2.4.7
    # via packaging
pytest-asyncio==0.14.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-pop==6.3
    # via -r requirements/tests.in
pytest-salt-factories==0.13.0
    # via pytest-pop
pytest-tempdir==2019.10.12
    # via pytest-salt-factories
pytest==6.2.2
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
    #   pytest-salt-factories
    #   pytest-tempdir
python-dateutil==2.8.1
    # via arrow
python-slugify==4.0.1
    # via cookiecutter
pyyaml==5.4.1
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   openapi3
    #   pop
    #   rend
pyzmq==22.0.3
    # via pytest-salt-factories
rend==6
    # via
    #   acct
    #   idem
requests==2.25.1
    # via
    #   cookiecutter
    #   openapi3
six==1.15.0
    # via
    #   cookiecutter
    #   python-dateutil
text-unidecode==1.3
    # via python-slugify
textwrap3==0.9.2
    # via -r requirements/base.txt
toml==0.10.2
    # via
    #   idem
    #   pytest
    #   rend
urllib3==1.26.4
    # via requests
wheel==0.36.2
    # via idem

{
    "project_name": "",
    "api_version": "latest",
    "service_name": "",
    "request_format": "",
    "plugins": {
        "service_name.sub.plugin_name": {
            "imports": ["import pathlib", "from sys import executable"],
            "virtual_imports": [],
            "func_alias": {"list_":  "list"},
            "virtualname": "",
            "doc": "a module level docstring",
            "functions":{
                "function_name": {
                    "doc": "a function docstring",
                    "hardcoded": {},
                    "return_type": "int",
                    "params":{
                        "param_name": {
                            "doc": "a parameter docstring",
                            "param_type": "int",
                            "required": "True",
                            "default": "None",
                            "target": "query_params",
                            "target_type": "mapping"
                        }
                    }

                }
            }
        }
    }
}
